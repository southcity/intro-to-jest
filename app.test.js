import { isLeapYear } from './app.js'

describe('A year is not supported', () => {
  test('A year is not null or undefined', () => {
    expect(() => isLeapYear()).toThrow('Invalid argument: year must be an integer')
  })

  test('A year is not negative', () => {
    expect(() => isLeapYear(-20)).toThrow('Invalid argument: year must be an integer equal to or larger than 0')
  })
})

describe('A year is a leap year', () => {
  test.each([1820, 1960, 2020])('Year %d is divisible by 4 but not by 100', (year) => {
    expect(isLeapYear(year)).toBeTruthy()
  })

  test('Year is divisible by 400', () => {
    expect(isLeapYear(2000)).toBeTruthy()
  })
})

describe('A year is not a leap year', () => {
  test('Year is not divisible by 4', () => {
    expect(isLeapYear(1981)).toBeFalsy()
  })

  test('Year is divisible by 100 but not by 400', () => {
    expect(isLeapYear(2100)).toBeFalsy()
  })
})
